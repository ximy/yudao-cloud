pipeline {
    agent {
        node {
            label "maven"
        }
    }


    parameters {
        choice(
            description: "你需要选择哪条分支进行构建?",
            name: "branch_name",
            choices: ["master"]
        )
        choice(
          description: "你需要选择哪个微服务模块进行构建?",
          name: "module_name",
          choices: ["yudao-gateway",
                    "yudao-module-bpm/yudao-module-bpm-biz",
                    "yudao-module-infra/yudao-module-infra-biz",
                    "yudao-module-system/yudao-module-system-biz",
                    ]
        )

    }
    environment {
        // 自建harbor仓库的namespace
        docker_hub_namespace = "yudao-project"

        // 自建镜像仓库地址
        docker_hub = "k8s-harbor:30002"

        // 在jenknis或kubesphere上面的凭证
        docker_hub_id = "yudao-project-harbor-id"

        // k8s 上面的 namespace
        k8s_namespace = "yudao-project"
        GIT_COMMIT_ID = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()

        // BUILD_NUMBER 这个变量从jenkins传递过来
        current_build_number = "${BUILD_NUMBER}"
    }

    stages {
        stage ("打印相关变量") {
            steps{
                echo "docker_hub_namespace信息为: ${docker_hub_namespace}"
                // 获取commit信息，用于后面打tag
                echo "commit信息为：${env.GIT_COMMIT_ID}"
                echo "current_build_number信息为：${env.current_build_number}"
                script {
                    // 因为传递过来的module有可能是youlai-admin/admin-boot形式的，打镜像时会失败
                    env.module_name_prefix = "${module_name}".split("/")[0]
                    env.module_name_suffix = "${module_name}".split("/")[-1]
                    // 本端tag名
                    env.local_tag = "${module_name_suffix}:${current_build_number}_${GIT_COMMIT_ID}"
                    // 远端tag名，必须以这种方式命令，才能push到远端
                    env.remote_tag = "${docker_hub}/${docker_hub_namespace}/${local_tag}"
                    echo "module_name信息为: ${module_name}"
                    echo "module_name_prefix信息为: ${module_name_prefix}"
                    echo "module_name_suffix信息为: ${module_name_suffix}"
                    echo "local_tag信息为：${env.local_tag}"
                    echo "remote_tag信息为：${env.remote_tag}"
                }
            }
        }

        stage("打包镜像") {
            steps {
                script {
                    container ('maven') {
                        sh "mvn clean install -Dmaven.test.skip=true"
                        sh "cd $module_name && docker build -t ${env.local_tag} -f ./Dockerfile ."
                        withCredentials([usernamePassword(
                            credentialsId: "${docker_hub_id}",
                            passwordVariable: 'DOCKER_PASSWORD',
                            usernameVariable: 'DOCKER_USERNAME')]){
                            sh 'echo "$DOCKER_PASSWORD" | docker login http://k8s-harbor:30002 -u "$DOCKER_USERNAME" --password-stdin'
                            sh "docker tag ${env.local_tag} ${env.remote_tag}"
                            sh "docker push  ${env.remote_tag}"
                        }
                    }
                }
            }
        }
        stage("部署至k8s") {
            agent none
            steps {
                container ("maven") {
                    // 这种方式启k8s是官方推荐的,此种方式，要求deploy.yaml里面不能有创建pvc的操作，否则会出错
                    sh "envsubst < $module_name/k8s/deploy.yaml | kubectl apply -f -"
                }
            }
        }
    }
}